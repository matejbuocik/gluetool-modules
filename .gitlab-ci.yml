---

stages:
  - test
  - deploy

# Use our custom image. See https://gitlab.com/testing-farm/images/-/tree/master/python-ci-image
image: "quay.io/testing-farm/python-ci-image:2023-05-23-73a71064"

#
# Poetry and MyPy colors
#
.colors:
  variables:
    TERM: "xterm"
    POETRY_ADDOPTS: "--ansi"
    MYPY_FORCE_COLOR: "1"
    PYTEST_ADDOPTS: "--color=yes"

#
# Common variables
#
variables:
  BUILDAH_TAG: v1.29.0

#
# Mark tests to run for merge requests
#
.merge-request:
  rules:
    - if: $CI_MERGE_REQUEST_IID

#
# TEST steps
#
.tests:
  extends: [.colors, .merge-request]
  before_script:
    - dnf -y install krb5-devel libcurl-devel popt-devel postgresql-devel libxml2-devel

# Build, test and expose the container image
container:
  extends: .merge-request
  stage: test
  image: quay.io/buildah/stable:$BUILDAH_TAG
  variables:
    GOSS_PATH: /usr/bin/goss
    GOSS_DOWNLOAD_URL: https://github.com/aelsabbahy/goss/releases/latest/download
  script:
    - dnf -y install make poetry python3.9 podman-docker
    - curl -L $GOSS_DOWNLOAD_URL/goss-linux-amd64 -o /usr/bin/goss && chmod +rx /usr/bin/goss
    - curl -L $GOSS_DOWNLOAD_URL/dgoss -o /usr/bin/dgoss && chmod +rx /usr/bin/dgoss
    - buildah login -u $QUAY_USERNAME -p $QUAY_PASSWORD quay.io
    - IMAGE_TAG=$CI_PIPELINE_ID make build
    - IMAGE_TAG=$CI_PIPELINE_ID make test-image
    - IMAGE_TAG=$CI_PIPELINE_ID make push


# Run unit tests
#
# Note: this step also generates coverage report (HTML).
py39-unit:
  extends: .tests
  stage: test
  script:
    - tox -v -e py39-unit-tests -- --cov=gluetool_modules_framework --cov-report=html:coverage-report
  artifacts:
    paths:
      - coverage-report

# Static analysis - pylint, flake8
py39-static:
  extends: .tests
  stage: test
  script:
    - tox -v -e py39-static-analysis

# Static analysis - coala
#
# Note: coala integration is better done directly via gitlab's docker support
static-coala:
  extends: .merge-request
  stage: test
  image: quay.io/testing-farm/coala:latest
  script:
    - /usr/bin/coala --non-interactive --config .coafile

# Static analysis - type checks
type-check:
  extends: [.colors, .merge-request]
  stage: test
  script:
    - tox -v -e type-check

# Static analysis - pre-commit
pre-commit:
  extends: .merge-request
  stage: test
  script:
    - dnf -y install pre-commit
    - poetry --version
    - pre-commit run --show-diff-on-failure --all-files

# Run all tmt plans via Testing Farm
testing-farm:
  extends: .merge-request
  stage: test
  image: quay.io/testing-farm/cli:latest
  script:
    - testing-farm request --git-ref $CI_MERGE_REQUEST_REF_PATH --git-url $CI_MERGE_REQUEST_PROJECT_URL

# Generate documentation from the sources
#
# Note: executed for all commits in all branches to make sure it is actually possible
# to generate the documentation - serves as a sort of a "test" on its own. citool
# uses docstrings to generate command-line help, it is useful to check whether these
# docstrings are readable and sane.
py39-generate-docs:
  extends: .tests
  stage: test
  script:
    - tox -v -e py39-doctest -- ./docs
  artifacts:
    paths:
      - .tox/py39-doctest/tmp/docs/build/html

#
# DEPLOY steps
#
publish/container:
  stage: deploy
  image: quay.io/buildah/stable:$BUILDAH_TAG
  script:
    - dnf -y install make poetry python3.9
    - buildah login -u $QUAY_USERNAME -p $QUAY_PASSWORD quay.io
    - IMAGE_TAG=latest make build
    - IMAGE_TAG=latest make push
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"
